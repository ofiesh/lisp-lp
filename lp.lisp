(defpackage :com.ofiesh.lp
  (:use :cl))

(in-package :com.ofiesh.lp)

(defgeneric getLinearObjectiveCoefficient (column)
  (:documentation "Gets the coefficient of a constraint for the linear objective function"))

(defgeneric getConstraintCoefficient (column constraint)
  (:documentation "Retrieves the coefficient for a given constraint, must return 0 if there is no coefficient"))

(defgeneric getRelationship (constraint)
  (:documentation "Retrieves the relationship of a constraint"))

(defgeneric solve (solver columns constraints &key goal)
  (:documentation "Minimizes on objective function with given constraints"))

(defclass slack-variable ()
  (constraint-coefficient
   objective-function-coefficient))

(defmethod initialize-instance :after ((slack-variable slack-variable) &key relationship)
  (setf (slot-value slack-variable 'coefficient) (if (eq relationship :gte) 1 -1))
  (setf (slot-value slack-variable 'objective-function-coefficient) 0))

(defclass constraint ()
  ((relationship
    :initarg :relationship)
   (slack-variable)))

(defmethod initialize-instance :after ((constraint constraint) &key)
  (setf (slot-value constraint 'slack-variable)
	(make-instance 'slack-variable 
		       :relationship (slot-value constraint 'relationship))))

(defmethod get-slack-coefficient ((constraint constraint))
  (slot-value (slot-value constraint 'slack-variable) 'constraint-coefficient))

(defclass simplex-solver () ())

(defclass simplex-solver-matrix ()
  ((columns
    :initarg :columns)
   (constraints
    :initarg :constraints)))

(defmethod initialize-instance :after ((matrix simplex-solver-matrix) &key)
  (format t "foobar"))
  

(defmethod solve ((solver simplex-solver) columns constraints &key goal)
  (make-instance 'simplex-solver-matrix 
		 :columns columns
		 :constraints constraints)
  (format t "~s" goal))